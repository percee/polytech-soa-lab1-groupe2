# Interfaces

## Avion

Le service avion est un service qui suit le paradigme RPC.
Il contient une méthode pour le moment getFlights() qui prends en paramètre un objet
métier Filtre qui contient:
 - L'aéroport de départ
 - L'aéroport d'arrivée
 - La date de l'aller(/retour)
 - Les préférences d'affichage (ordre, vol direct uniquement, etc)
 

Cette méthode renvoie donc la liste des vols correspondants à ces différents paramètres

RPC à été choisi pour cette interface car il est nécessaire lors de l'execution de la recherche 
des vols d'utiliser une logique métier poussée basée sur un appel de procédure. De plus, le fait que nous soyons en stateful 
permet d'avoir facilement une recherche basée sur l'utilisateur et ses préférences. En effet, le fait
d'avoir une session entre le client et notre service permet d'obtenir plus facilement ses préférences.


## Hotel && Voiture
Les services hotel et Voiture sont des services qui suivent le paradigme ressource.
Ils contiennent une méthode getHotelList() (Hotel) et getVoitureList() qui permettent d'obtenir leur liste complête si on effectue une requête GET simple sur /hotel ou /voiture.
Lorsque l'on ajoute des paramètres, le lieu en premier puis la date, cette liste est filtrée en fonction des disponibilités.
Ainsi, si une requête GET est envoyée sur /hotel/paris/08-10-2017, la liste des hotels parisiens disponibles à cette date sera renvoyée. 
Pour le service voiture, le paramètre de durée de location peut être ajouté comme étant un entier, un nombre de jours de location.

Ressource à été choisi pour ces interface car elles correspondent tout à fait à une approche 
ressource. En effet, on va aller chercher une liste brute d'hotels ou de voiture ce qui est une simple ressource
et qui ne nécessite pas de logique métier particulière.


## Mailer

Le mailer est un service qui a une interface suivant le paradigme RPC.
Il fonctionne en appelant la méthode sendMail() en passant un objet Mail en paramètre.
L'objet Mail contient 3 strings: 
- l'adresse du destinataire
- l'objet du message
- le corps du message

La méthode sendMail() renvoie un objet MailStatus contenant un booléen 
qui est vrai si le mail a bien été envoyé, faux sinon.

Nous avons choisi RPC pour cette interface car c'est une interface qui sujet à très peu de changements,
donc l'effet du couplage fort de ce paradigme est minimisé.
RPC permet aussi une consommation simple du service grâce aux descriptions de services.

Le mailer peut difficilement suivre la philosophie ressource,
et le paradigme document serait plus adapté si le mailer pouvait effectuer plusieurs actions différentes.

## Travel

Travel est un service qui a une interface suivant le paradigme document.
Il fonctionne en gardant en mémoire des maps des différentes demandes de voyage.
L'objet travel request contient:
- les détails de la personne
- les flights demandés
- les hotel stays demandés
- les car rentals demandés

Les messages qu'il est possible d'envoyer sont:
- SUBMIT pour envoyer une nouvelle demande de travel
- VALIDATE pour accepter une demande en pending, et la passer en approved
- REFUSE pour refuser une demande en pending et la supprimer
- SUMMARY pour obtenir les détails d'une demande dont l'id est donnée
- LIST pour obtenir une list des demandes existantes, à la fois pending et approved
- PURGE pour supprimer toutes les requêtes existantes et réinitialiser les id 


# Design Choices

## RPC

Pour RPC, nous avons choisi de suivre une approche "Code first" pour deux raisons majeurs:
    -Cela nous permet d'écrire du code et non pas du XML qui est pour nous difficile à relire 
    et également à écrire.
    - Le versionning de fichiers XML est complexe, il n'y a qu'a imaginer le cas ou deux personnes venaient
    à travailler en même temps sur un contrat, le merge pourrait être très douloureux là ou en code first,
    nous avons l'habitude de gérer des conflits
    
## Ressource

Pour ressource, les choix de design majeurs sont plus du au métier et à la logique globale de notre système.
Comme nous savons déjà que par la suite le service Travel va chercher des hotels et des locations de voitures
en fonction des villes d'arrivés et de départ, nous avons exposé des interfaces permettant d'exposer ces
ressources par ville afin de faciliter l'intégration future.
    

## Document

Nous avons choisi Document pour cette interface car on peut facilement imaginer une extension future,
par exemple pour rajouter un type de message permettant de modifier une requête en pending au cas où
une erreur de frappe a été commise. Il est plus facile de rajouter cette fonctionnalité dans le cas de
Document car il suffit de rajouter un type d'évènement.

De plus certains paramètres sont optionnels. Par exemple pour l'event LIST, on peut spécifier de ne lister
que les pending, les approved, ou les deux à la fois. Le paradigme Document est celui qui gère
le plus simplement ces cas-là, il suffit de spécifier ou non un attribut dans l'objet Json.

Nous avons mis en place des tests d'acceptation sur ce service car c'est le plus "complexe", dans le sens 
ou c'est le seul a avoir un stockage de données et qu'on veut vérifier que les bons objets sont enregistrés
selon les commandes envoyées.
