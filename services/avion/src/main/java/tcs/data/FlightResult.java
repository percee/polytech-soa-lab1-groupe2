package tcs.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class FlightResult {

	private Flight[] flights;

	@XmlElement
	public Flight[] getFlights() { return flights; }
	public void setFlights(Flight[] flights) { this.flights = flights; }

}
