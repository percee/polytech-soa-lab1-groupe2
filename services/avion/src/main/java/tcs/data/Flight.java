package tcs.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlType
public class Flight {

    private Date date;
    private int duration;
    private double price;
    private String company;
    private String src;
    private String dest;



    @XmlElement
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    @XmlElement
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @XmlElement
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    @XmlElement
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    @XmlElement
    public String getSrc() {
        return src;
    }
    public void setSrc(String src) {
        this.src = src;
    }

    @XmlElement
    public String getDest() {
        return dest;
    }
    public void setDest(String dest) {
        this.dest = dest;
    }


}