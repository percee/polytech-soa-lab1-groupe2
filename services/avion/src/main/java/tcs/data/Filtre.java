package tcs.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import java.util.Date;
import java.util.List;

@XmlType
public class Filtre {

	private String src;
	private String dest;
	private Date aller;

	private Date retour;
	private PriceOrder priceorder;
	private List<String> companies;

	private boolean directFlight;
	private int maxTravelTime; //en minutes

	@XmlElement(required = true)
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}

	@XmlElement(required = true)
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}

	@XmlElement
	public Date getAller() {
		return aller;
	}
	public void setAller(Date aller) {
		this.aller = aller;
	}

	@XmlElement
	public Date getRetour() {
		return retour;
	}
	public void setRetour(Date retour) {
		this.retour = retour;
	}

	@XmlElement
	public PriceOrder getPriceorder() {
		return priceorder;
	}
	public void setPriceorder(PriceOrder priceorder) {
		this.priceorder = priceorder;
	}

	@XmlElement
	public List<String> getCompanies() {
		return companies;
	}
	public void setCompanies(List<String> companies) {
		this.companies = companies;
	}

	@XmlElement
	public boolean isDirectFlight() {
		return directFlight;
	}
	public void setDirectFlight(boolean directFlight) {
		this.directFlight = directFlight;
	}

	@XmlElement
	public int getMaxTravelTime() {
		return maxTravelTime;
	}
	public void setMaxTravelTime(int maxTravelTime) {
		this.maxTravelTime = maxTravelTime;
	}


	@Override
	public String toString() {
		return "Filtre{" +
				"src='" + src + '\'' +
				", dest='" + dest + '\'' +
				", aller=" + aller +
				", retour=" + retour +
				", priceorder=" + priceorder +
				", companies=" + companies +
				", directFlight=" + directFlight +
				", maxTravelTime=" + maxTravelTime +
				'}';
	}
}
