package tcs.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import tcs.data.*;

@WebService(name="Avion", targetNamespace = "http://localhost/services/avion")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AvionService {

	@WebResult(name="flight_list")
	FlightResult getFlights(@WebParam(name="filtres") Filtre request);

}
