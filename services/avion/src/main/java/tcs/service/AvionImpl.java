package tcs.service;

import tcs.data.*;

import javax.jws.WebService;
import java.util.Date;


@WebService(targetNamespace   = "http://localhost/services/avion",
		    portName          = "ExternalAvionsPort",
		    serviceName       = "ExternalAvionsService",
		    endpointInterface = "tcs.service.AvionService")
public class AvionImpl implements AvionService {

	public FlightResult getFlights(Filtre filtre) {
		//Get a list of flights based on filter
        Flight[] flights = getFilteredFlights();
		return buildResponse(flights);
	}

	private FlightResult buildResponse(Flight[] flights) {
		FlightResult result = new FlightResult();
		result.setFlights(flights);
		return result;
	}

	/***************************************
	 ** Mock for the Business Logic Layer **
	 ***************************************/
	private Flight[] getFilteredFlights() {

		Flight test = new Flight();
		test.setDate(new Date());
		test.setCompany("Air France");
		test.setDest("Paris");
		test.setSrc("London");
		test.setDuration(120);
		test.setPrice(49.99);
		Flight[] flights =  new Flight[3];
		flights[0] = test;
		flights[1] = test;
		flights[2] = test;

		return flights;
	}

}
