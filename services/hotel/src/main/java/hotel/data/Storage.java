package hotel.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Storage {

	// this mocks a database.
	private static ArrayList<Hotel> hotels = new ArrayList<>();

	public static void fillDatabase() {
		SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
        
		Hotel h1 = new Hotel("Nuit paisible", "Paris", 48, 5);
		Hotel h2 = new Hotel("Fleurette", "Paris", 41, 5);
		try {
			h2.book(parser.parse("01-01-2017"), 5);
		} catch (ParseException e) {
		}
		Hotel h3 = new Hotel("BNB Nice", "Nice", 38, 5);
		try {
			h3.book(parser.parse("01-01-2017"), 5);
		} catch (ParseException e) {
		}
		Hotel h4 = new Hotel("Lac Lément", "Nice", 40, 5);
		
		hotels.add(h1);
		hotels.add(h2);
		hotels.add(h3);
		hotels.add(h4);
	}

	public static ArrayList<Hotel> getHotelList(){
		Collections.sort(hotels);
		return hotels;
	}
	
	public static ArrayList<Hotel> getHotelByDestination(ArrayList<Hotel> givenHotels, String destination){
		ArrayList<Hotel> liste = new ArrayList<Hotel>();
		for(Hotel h : givenHotels){
			if(h.getLocation().toLowerCase().equals(destination.toLowerCase())){
				liste.add(h);
			}
		}
		return liste;
	}
	
	public static ArrayList<Hotel> getHotelByDate(ArrayList<Hotel> givenHotels, String stringdate) throws ParseException{
		ArrayList<Hotel> liste = new ArrayList<Hotel>();
		SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
        Date date = parser.parse(stringdate);
		
		for(Hotel h : givenHotels){
			if(h.isAvailableOnDate(date)){
				liste.add(h);
			}
		}
		return liste;
	}
	
	static {
		Storage.fillDatabase();
	}

}