package hotel.data;

import java.util.Date;
import java.util.HashMap;

public class  Hotel implements Comparable<Hotel> {

	private static int nextId =0;
	
	private int id;
	private String name;
	private String location;
	private double priceByNight;
	private int maximumCapacity;
	private HashMap<Date, Integer> bookedNights;
	

	public Hotel(String name, String location, double priceByNight, int maximumCapacity) {
		this.id = nextId++;
		this.name = name;
		this.location = location;
		this.priceByNight = priceByNight;
		this.maximumCapacity = maximumCapacity;
		bookedNights = new HashMap<Date, Integer>();
	}
	
	public int getId(){
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}


	public double getPriceByNight() {
		return priceByNight;
	}


	public void setPriceByNight(double priceByNight) {
		this.priceByNight = priceByNight;
	}
	
	public int getMaximumCapacity() {
		return maximumCapacity;
	}
	
	public boolean book(Date date, int numberOfPerson){
		if(numberOfPerson <= 0) return false;
		if(bookedNights.containsKey(date)){
			int placeTaken = bookedNights.get(date)+numberOfPerson;
			
			if(placeTaken > maximumCapacity){
				return false;
			}
			bookedNights.put(date, placeTaken);
		}else {
			bookedNights.put(date, numberOfPerson);
		}
		return true;
	}

	public boolean isAvailableOnDate(Date date){
		Integer placeTaken = bookedNights.get(date);
		if(placeTaken != null && placeTaken >= maximumCapacity) 
			return false;
		
		return true;
	}

	@Override
	public int compareTo(Hotel arg0) {
		return new Double(priceByNight).compareTo(arg0.getPriceByNight());
	}

}
