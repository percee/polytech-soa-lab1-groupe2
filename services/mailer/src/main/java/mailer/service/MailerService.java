package mailer.service;

import mailer.data.Mail;
import mailer.data.MailStatus;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(name="Mailer", targetNamespace = "localhost")
public interface MailerService {

    @WebResult(name="simple_result")
    MailStatus sendMail(@WebParam(name="mail") Mail mail);
}
