package mailer.service;

import mailer.data.Mail;
import mailer.data.MailStatus;
import org.apache.commons.validator.routines.EmailValidator;

import javax.jws.WebService;

@WebService(targetNamespace   = "localhost",
        portName          = "MailerPort",
        serviceName       = "MailerService",
        endpointInterface = "mailer.service.MailerService")
public class MailerImpl implements MailerService {

	public MailStatus sendMail(Mail mail) {
        MailStatus status = new MailStatus();
        if(EmailValidator.getInstance().isValid(mail.getRecipient())){
            System.out.println("Sending mail:");
            System.out.println(mail);
            status.setStatus(true);
            return status;
        } else {
            System.out.println("Invalid address: " + mail.getRecipient());
            status.setStatus(false);
            return status;
        }

    }

}
