package mailer.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Mail{

	private String recipient;
	private String subject;
	private String message;

    @XmlElement(required = true)
    public String getRecipient() { return recipient; }
    public void setRecipient(String recipient) { this.recipient = recipient; }

    @XmlElement(required = true)
    public String getSubject() { return subject; }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @XmlElement(required = true)
    public String getMessage() { return message; }
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return  "\n  to: " + recipient +
                "\n  subject: " + subject +
                "\n  message: " + message;
    }
}
