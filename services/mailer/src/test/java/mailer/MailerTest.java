package mailer;

import mailer.data.Mail;
import mailer.data.MailStatus;
import mailer.service.MailerImpl;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test cases for the Mailer
 *
 * @author Robin Alonzo
 */
public class MailerTest {

    @Test
    public void sendValidEmail(){
        MailerImpl mailer = new MailerImpl();
        Mail mail = new Mail();
        mail.setRecipient("example@example.com");
        mail.setSubject("Subject example");
        mail.setMessage("This is an example.");
        MailStatus status = mailer.sendMail(mail);
        assertTrue(status.getStatus());
    }

    @Test
    public void sendInvalidEmail(){
        MailerImpl mailer = new MailerImpl();
        Mail mail = new Mail();
        mail.setRecipient("no");
        mail.setSubject("Subject example");
        mail.setMessage("This is an example.");
        MailStatus status = mailer.sendMail(mail);
        assertFalse(status.getStatus());
    }

}
