package travel.entities;

import org.json.JSONObject;

public class HotelStay {

    private String city;
    private String hotel;
    private String dateStart;
    private String dateEnd;
    private int price;

    public HotelStay(){}

    public HotelStay(JSONObject data){
        this.city = data.getString("city");
        this.hotel = data.getString("hotel");
        this.dateStart = data.getString("date_start");
        this.dateEnd = data.getString("date_end");
        this.price = data.getInt("price");
    }

    JSONObject toJson() {
        return new JSONObject()
                .put("city", city)
                .put("hotel", hotel)
                .put("date_start", dateStart)
                .put("date_end", dateEnd)
                .put("price", price);
    }
}
