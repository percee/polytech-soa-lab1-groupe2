package travel.entities;

import org.json.JSONObject;

public class Flight {

    private String date;
    private String duration;
    private int price;
    private String company;
    private String originCity;
    private String destinationCity;

    public Flight(){}

    public Flight(JSONObject data){
        this.date = data.getString("date");
        this.duration = data.getString("duration");
        this.price = data.getInt("price");
        this.company = data.getString("company");
        this.originCity = data.getString("origin_city");
        this.destinationCity = data.getString("destination_city");
    }

    JSONObject toJson() {
        return new JSONObject()
                .put("date", date)
                .put("duration", duration)
                .put("price", price)
                .put("company", company)
                .put("origin_city", originCity)
                .put("destination_city", destinationCity);
    }
}
