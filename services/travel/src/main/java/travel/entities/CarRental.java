package travel.entities;

import org.json.JSONObject;

public class CarRental {

    public String city;
    public String shop;
    public String duration;
    public int price;

    public CarRental(){}

    public CarRental(JSONObject data){
        this.city = data.getString("city");
        this.shop = data.getString("shop");
        this.duration = data.getString("duration");
        this.price = data.getInt("price");
    }

    JSONObject toJson() {
        return new JSONObject()
                .put("city", city)
                .put("shop", shop)
                .put("duration", duration)
                .put("price", price);
    }
}
