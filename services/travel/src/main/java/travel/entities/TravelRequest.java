package travel.entities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TravelRequest {

    private String firstName;
    private String lastName;
    private List<Flight> flights;
    private List<CarRental> carRentals;
    private List<HotelStay> hotelStays;

    public TravelRequest() {}

    public TravelRequest(JSONObject data) {
        flights = new ArrayList<>();
        carRentals = new ArrayList<>();
        hotelStays = new ArrayList<>();
        this.lastName = data.getString("last_name");
        this.firstName = data.getString("first_name");

        if (data.has("flights"))
        {
            for (Object o: data.getJSONArray("flights")){
                if ( o instanceof JSONObject ){
                    flights.add(new Flight((JSONObject)o));
                }
            }
        }

        if (data.has("car_rentals"))
        {
            for (Object o: data.getJSONArray("car_rentals")){
                if ( o instanceof JSONObject ){
                    carRentals.add(new CarRental((JSONObject)o));
                }
            }
        }

        if (data.has("hotel_stays"))
        {
            for (Object o: data.getJSONArray("hotel_stays")){
                if ( o instanceof JSONObject ){
                    hotelStays.add(new HotelStay((JSONObject)o));
                }
            }
        }
    }


    public JSONObject toJson() {
        JSONArray f = new JSONArray();
        for (Flight flight: flights){
            f.put(flight.toJson());
        }


        JSONArray c = new JSONArray();
        for (CarRental carRental: carRentals)
            c.put(carRental.toJson());

        JSONArray h = new JSONArray();
        for (HotelStay hotelStay: hotelStays)
            h.put(hotelStay.toJson());

        return new JSONObject()
                .put("last_name", lastName)
                .put("first_name", firstName)
                .put("flights", f)
                .put("car_rentals", c)
                .put ("hotel_stays", h);
    }

}
