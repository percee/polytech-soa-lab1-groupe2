package travel;

import org.json.JSONObject;
import travel.entities.TravelRequest;

import java.util.Map;


class Handler {

    static JSONObject submit(JSONObject input) {
        TravelRequest request = new TravelRequest(input.getJSONObject("request"));
        int id = Storage.addPending(request);
        return new JSONObject().put("submit", "done").put("id", id);
    }

    static JSONObject validate(JSONObject input) {
        int id = input.getInt("id");
        boolean validated = Storage.validatePending(id);
        return new JSONObject().put("validate", "done");
    }

    static JSONObject refuse(JSONObject input) {
        int id = input.getInt("id");
        boolean validated = Storage.refusePending(id);
        return new JSONObject().put("refuse", "done");
    }

    static JSONObject summary(JSONObject input) {
        int id = input.getInt("id");
        TravelRequest request = Storage.getById(id);
        if (request == null)
            return new JSONObject().put("found", false);
        return new JSONObject().put("found", true).put("request", request.toJson());
    }

    static JSONObject list(JSONObject input) {
        boolean pending = input.has("pending") && input.getBoolean("pending");
        boolean approved = input.has("approved") && input.getBoolean("approved");

        if (!pending && !approved)
            pending = approved = true;

        JSONObject result = new JSONObject();

        if (pending){
            Map<Integer, TravelRequest> requests = Storage.getPending();
            JSONObject tmp = new JSONObject();
            for (Integer id : requests.keySet()){
                tmp.put(String.valueOf(id), requests.get(id).toJson());
            }
            result.put("pending", tmp);
        }

        if (approved){
            Map<Integer, TravelRequest> requests = Storage.getApproved();
            JSONObject tmp = new JSONObject();
            for (Integer id : requests.keySet()){
                tmp.put(String.valueOf(id), requests.get(id).toJson());
            }
            result.put("approved", tmp);
        }

        return result;
    }

    static JSONObject purge(JSONObject input) {
        JSONObject result = new JSONObject();

        Storage.purge();

        result.put("purge", "done");

        return result;
    }
}
