package travel;

public enum EVENT {
    SUBMIT, VALIDATE, REFUSE, SUMMARY, LIST, PURGE
}
