package travel;

import travel.entities.TravelRequest;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private static Map<Integer, TravelRequest> pending = new HashMap<Integer, TravelRequest>();
    private static Map<Integer, TravelRequest> approved = new HashMap<Integer, TravelRequest>();
    private static int nextId = 1;

    public static void purge() {
        pending.clear();
        approved.clear();
        nextId = 1;
    }

    public static int addPending(TravelRequest request) {
        pending.put(nextId, request);
        return nextId++;
    }

    public static boolean validatePending(int id) {
        if (pending.containsKey(id)) {
            approved.put(id, pending.get(id));
            pending.remove(id);
            return true;
        }
        return false;
    }

    public static boolean refusePending(int id) {
        if (pending.containsKey(id)) {
            pending.remove(id);
            return true;
        }
        return false;
    }

    public static TravelRequest getById(int id) {
        if (pending.containsKey(id))
            return pending.get(id);
        if (approved.containsKey(id))
            return approved.get(id);
        return null;
    }

    public static Map<Integer, TravelRequest> getPending(){
        return pending;
    }

    public static Map<Integer, TravelRequest> getApproved(){
        return approved;
    }
}