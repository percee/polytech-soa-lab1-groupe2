package travel;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class Travel {

    private static final int INDENT_FACTOR = 2;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response process(String input) {
        JSONObject obj = new JSONObject(input);

        try {
            switch (EVENT.valueOf(obj.getString("event"))) {
                case SUBMIT:
                    return Response.ok().entity(Handler.submit(obj).toString(INDENT_FACTOR)).build();
                case VALIDATE:
                    return Response.ok().entity(Handler.validate(obj).toString(INDENT_FACTOR)).build();
                case REFUSE:
                    return Response.ok().entity(Handler.refuse(obj).toString(INDENT_FACTOR)).build();
                case SUMMARY:
                    return Response.ok().entity(Handler.summary(obj).toString(INDENT_FACTOR)).build();
                case LIST:
                    return Response.ok().entity(Handler.list(obj).toString(INDENT_FACTOR)).build();
                case PURGE:
                    return Response.ok().entity(Handler.purge(obj).toString(INDENT_FACTOR)).build();
            }
        }catch(Exception e) {
            JSONObject error = new JSONObject().put("error", e.toString());
            return Response.status(400).entity(error.toString(INDENT_FACTOR)).build();
        }
        return null;
    }

}
