package voiture.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Storage {

	// this mocks a database.
	private static ArrayList<LoueurVoiture> loueurs = new ArrayList<>();

	public static void fillDatabase() {
		SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
        
		LoueurVoiture h1 = new LoueurVoiture("Mercedes", "Paris", 48, 5);
		LoueurVoiture h2 = new LoueurVoiture("Mega Loueur", "Paris", 41, 5);
		try {
			h2.book(parser.parse("01-01-2017"), 2, 5);
		} catch (ParseException e) {
		}
		LoueurVoiture h3 = new LoueurVoiture("Nice Voiture", "Nice", 38, 5);
		try {
			h3.book(parser.parse("01-01-2017"), 2, 5);
		} catch (ParseException e) {
		}
		LoueurVoiture h4 = new LoueurVoiture("Audi Location", "Nice", 40, 5);
		
		loueurs.add(h1);
		loueurs.add(h2);
		loueurs.add(h3);
		loueurs.add(h4);
	}

	public static ArrayList<LoueurVoiture> getHotelList(){
		return loueurs;
	}
	
	public static ArrayList<LoueurVoiture> getHotelByDestination(ArrayList<LoueurVoiture> givenRenters, String destination){
		ArrayList<LoueurVoiture> liste = new ArrayList<LoueurVoiture>();
		for(LoueurVoiture h : givenRenters){
			if(h.getLocation().toLowerCase().equals(destination.toLowerCase())){
				liste.add(h);
			}
		}
		return liste;
	}
	
	public static ArrayList<LoueurVoiture> getHotelByDate(ArrayList<LoueurVoiture> givenHotels, String stringdate) throws ParseException{
		ArrayList<LoueurVoiture> liste = new ArrayList<LoueurVoiture>();
		SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
        Date date = parser.parse(stringdate);
		
		for(LoueurVoiture h : givenHotels){
			if(h.isAvailableOnDate(date)){
				liste.add(h);
			}
		}
		return liste;
	}
	
	public static ArrayList<LoueurVoiture> getHotelByDateAndDuration(ArrayList<LoueurVoiture> givenHotels, String stringdate, int duration) throws ParseException{
		ArrayList<LoueurVoiture> liste = new ArrayList<LoueurVoiture>();
		SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
        Date date = parser.parse(stringdate);
		
		for(LoueurVoiture h : givenHotels){
			if(h.isAvailableOnDateAndDuration(date, duration)){
				liste.add(h);
			}
		}
		return liste;
	}
	
	static {
		Storage.fillDatabase();
	}

}