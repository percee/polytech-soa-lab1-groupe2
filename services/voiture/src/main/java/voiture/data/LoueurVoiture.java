package voiture.data;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class  LoueurVoiture {

	private static int nextId =0;
	
	private int id;
	private String name;
	private String location;
	private double dailyPrice;
	private int maxCarNumber;
	private HashMap<Date, Integer> remainingCars;
	

	public LoueurVoiture(String name, String location, double dailyPrice, int maxCarNumber) {
		this.id = nextId++;
		this.name = name;
		this.location = location;
		this.dailyPrice = dailyPrice;
		this.maxCarNumber = maxCarNumber;
		remainingCars = new HashMap<Date, Integer>();
	}
	
	public int getId(){
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}


	public double getDailyPrice() {
		return dailyPrice;
	}


	public void setDailyPrice(double dailyPrice) {
		this.dailyPrice = dailyPrice;
	}
	
	public int getMaxCarNumber() {
		return maxCarNumber;
	}
	
	public boolean book(Date date, int duration, int numberOfCars){
		if(numberOfCars <= 0 || numberOfCars > maxCarNumber) return false;
		
		Date currentDate = date;
		for(int i = 0; i < duration; i++){
			Calendar c = Calendar.getInstance(); 
			c.setTime(date); 
			c.add(Calendar.DATE, i);
			currentDate = c.getTime();
			
			if(remainingCars.containsKey(currentDate)){
				int carLeft = remainingCars.get(currentDate) - numberOfCars;
				
				if(carLeft < 0){
					return false;
				}
				remainingCars.put(currentDate, carLeft);
			}else {
				remainingCars.put(currentDate, maxCarNumber - numberOfCars);
			}
		}
		return true;
	}
	
	public boolean isAvailableOnDate(Date date){
		return isAvailableOnDateAndDuration(date, 1);
	}

	public boolean isAvailableOnDateAndDuration(Date date, int duration){
		Date currentDate = date;
		for(int i = 0; i < duration; i++){
			Calendar c = Calendar.getInstance(); 
			c.setTime(date); 
			c.add(Calendar.DATE, i);
			currentDate = c.getTime();
			
			Integer carLeft = remainingCars.get(currentDate);
			if(carLeft != null && carLeft <= 0) 
				return false;
		}
		return true;
	}


}
