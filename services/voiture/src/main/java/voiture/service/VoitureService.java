package voiture.service;

import org.json.JSONArray;
import org.json.JSONObject;

import voiture.data.LoueurVoiture;
import voiture.data.Storage;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.text.ParseException;
import java.util.ArrayList;

@Path("/")
// Here we generate JSON data from scratch, one should use a framework instead
@Produces(MediaType.APPLICATION_JSON)
public class VoitureService {
/*
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	public Response createNewGenerator(String name) {
	    if(Storage.read(name) != null) {
			return Response.status(Response.Status.CONFLICT)
					       .entity("\"Existing name " + name + "\"")
					       .build();
		}
		Storage.create(name);
		return Response.ok().build();
	}
*/
	@GET
	public Response getHotelList() {
		ArrayList<LoueurVoiture> hotels = Storage.getHotelList();
		JSONArray result = convertToJson(hotels);
		return Response.ok().entity(result.toString(2)).build();
	}


	@Path("/{destination}")
	@GET
	public Response getHotelByDestination(@PathParam("destination") String destination) {
		ArrayList<LoueurVoiture> hotels = Storage.getHotelList();
		hotels = Storage.getHotelByDestination(hotels, destination);
		
		JSONArray result = convertToJson(hotels);
		return Response.ok().entity(result.toString(2)).build();
	}

	@Path("/{destination}/{date}")
	@GET
	public Response deleteGenerator(@PathParam("destination") String destination, @PathParam("date") String date) {
		ArrayList<LoueurVoiture> hotels = Storage.getHotelList();
		hotels = Storage.getHotelByDestination(hotels, destination);
		try {
			hotels = Storage.getHotelByDate(hotels, date);
		} catch (ParseException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		JSONArray result = convertToJson(hotels);
		return Response.ok().entity(result.toString(2)).build();
	}
	
	@Path("/{destination}/{date}/{duration}")
	@GET
	public Response deleteGenerator(@PathParam("destination") String destination, @PathParam("date") String date, @PathParam("duration") int duration) {
		ArrayList<LoueurVoiture> hotels = Storage.getHotelList();
		hotels = Storage.getHotelByDestination(hotels, destination);
		try {
			hotels = Storage.getHotelByDateAndDuration(hotels, date, duration);
		} catch (ParseException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		JSONArray result = convertToJson(hotels);
		return Response.ok().entity(result.toString(2)).build();
	}
	
	

	public static JSONArray convertToJson(ArrayList<LoueurVoiture> liste){
		JSONArray array = new JSONArray();
		for(LoueurVoiture h : liste){
			JSONObject obj = new JSONObject();
			obj.put("id", h.getId());
			obj.put("name", h.getName());
			obj.put("price", h.getDailyPrice());
			array.put(obj);
		}
		return array;
	}

}
