# Planning

Groupe 2

## Semaine 1 - Découverte des ESB et première intégration de services

**Cas d'utilisation**

- *Obtenir l'hôtel le moins cher parmi deux services d'hôtels*: intégrer deux services qui font la même chose mais n’ont pas forcément le même format de données.
- *Soumettre une TravelRequest*: l’employé doit pouvoir soumettre une demande minimale ne contenant potentiellement qu’un hôtel.
- *Pouvoir approuver une TravelRequest en attente (pending)*: un manager doit pouvoir récupérer la liste des requêtes qui sont en pending et les accepter.

Nous prévoyons de terminer le travail que nous n’avons pas achevé lors du premier rendu, surtout les tests d’acceptation qui nous permettront d’avancer sans régresser.

À la fin de la semaine, on espère avoir compris comment implémenter une intégration de service, et avoir un POC.

## Semaine 2 - Finir le fonctionnement minimal avec remboursement

**Cas d'utilisation**

- *Obtenir la location de voiture la moins cher parmi deux services de location*
- *Obtenir le vol le moins cher parmi deux services de vols*: deux cas d’utilisations triviaux car très semblable à celui pour les deux services d’hôtels, que nous auront déjà réalisé.
- *Envoyer des documents justifiant des dépenses de vie*: une fonctionnalité qui nécessite d’enregistrer un document/une image et de mettre a jour une TravelRequest.


**User stories**

- *En tant qu’employé je souhaite être remboursé à la fin de mon voyage*: nous voulons finir le plus vite possible le fonctionnement entier d’un MVP, de la demande de voyage au remboursement. C’est une fonctionnalité importante pour la démo.
- *En tant qu’employé je souhaite être notifié par mail lorsqu’une travel request dépasse le seuil afin que je sache qu’il faut une justification*: pour le MVP, nous diront que si la requête est sous le seuil elle est remboursée, sinon elle ne l’est pas et l’employé est averti. Pas encore de justifications demandée.

## Semaine 3 - Les justifications de dépenses de vie

**Cas d'utilisation**

- *Obtenir un rapport détaillé (vol, hotel, voiture, dépenses) sur une TravelRequest en cours*
- *Envoyer une justification pour une travel request qui a dépassé le seuil*
- *Pouvoir approuver le remboursement d’une travel request qui a dépassé le seuil.*

Cette semaine sera concentrée sur l’ajout des justifications de dépenses dans les requêtes, avec la possibilité pour le manager de les lire, et d’accepter ou refuser manuellement une requête ayant dépassé le seuil.

## Semaine 4 - Finalisation

**Cas d'utilisation**

- *Obtenir les pièces enregistrées liées à une travel request.*: fonctionnalité pas fondamentale au fonctionnement de l’application, récupérer les documents/images envoyés par les employés reste néanmoins important.

**User stories**

- *En tant que manager je souhaite que les travel requests remboursés soient archivés afin de pouvoir avoir accès à de vieilles données*

Cette semaine est plus légère que les autre, nous gardons un peu de temps pour compenser les retards qui risquent de s’accumuler les semaines précédentes.