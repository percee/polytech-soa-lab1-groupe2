#!/bin/sh

#PUSH=true
PUSH=false

build() { # $1: directory, $2: image_name
  cd $1
  echo "Building" $1
  docker build -t $2 .
  if [ "$PUSH" = "true" ]; then docker push $2; fi
  cd ..
}

# Compile services code
mvn clean package

# Build docker images
build car       third-party-car
build flights third-party-flights
build hotels  third-party-hotels
