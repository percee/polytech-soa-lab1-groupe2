#!/bin/sh

excludeIntegration=$(docker ps -aq --filter='name=bus')
excludeDatabase=$(docker ps -aq --filter='name=mongo:3.4.9')
table=($(docker ps -aq))
size=${#table[@]}

while true
do
    rand=$((RANDOM % $size))
    while [ ${table[$rand]} == ${excludeIntegration}  ] 
    do
        rand=$((RANDOM % $size))
    done
    nameStop=$(docker ps --filter id=${table[$rand]} --format "{{.Image}}");
    echo "The monkey stop $nameStop"
    docker stop ${table[$rand]}
    sleep 40s

    rand=$((RANDOM % $size))
    while [ ${table[$rand]} == ${excludeIntegration}  ] 
    do
        rand=$((RANDOM % $size))
    done
    nameStart=$(docker ps --filter id=${table[$rand]} --format "{{.Image}}");
    echo "The monkey start $nameStart"
    docker start ${table[$rand]}
    sleep 40s
done