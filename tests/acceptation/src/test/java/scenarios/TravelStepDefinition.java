package scenarios;

import cucumber.api.java.en.*;

import javax.ws.rs.core.MediaType;
import org.apache.cxf.jaxrs.client.WebClient;
import static org.junit.Assert.*;

import org.json.JSONArray;
import org.json.JSONObject;


public class TravelStepDefinition {


    private String host = "localhost";
    private int port = 9060;

    private JSONObject request;
    private JSONArray flights;
    private JSONArray carRentals;
    private JSONArray hotelStays;
    private JSONObject answer;
    private String ssn;
    private String filter;
    private String safeWord;

    private JSONObject call(JSONObject request) {
        String raw =
                WebClient.create("http://" + host + ":" + port + "/travel/")
                        .accept(MediaType.APPLICATION_JSON_TYPE)
                        .header("Content-Type", MediaType.APPLICATION_JSON)
                        .post(request.toString(), String.class);
        return new JSONObject(raw);
    }

    @Given("^an empty travel registry deployed on (.*):(\\d+)$")
    public void set_clean_registry(String host, int port) {
        this.host = host;
        this.port = port;
        JSONObject ans = call(new JSONObject().put("event", "PURGE"));
        assertEquals("done", ans.getString("purge"));
    }


    @Given("^a request for (John|Jane) added to the registry$")
    public void upload_preregistered_request(String name) {
        JSONObject request = new JSONObject();
        if (name.equals("John")) {
            request.put("first_name", "John")
                    .put("last_name", "Johnson");
        } else {
            request.put("first_name", "Jane")
                    .put("last_name", "Janeson");
        }
        JSONObject ans = call(new JSONObject().put("event", "SUBMIT").put("request", request));
        assertEquals("done", ans.getString("submit"));
    }


    @Given("^a travel request")
    public void initialize_a_request() {
        request = new JSONObject();
        flights = new JSONArray();
        carRentals = new JSONArray();
        hotelStays = new JSONArray();
    }

    @Given("^a flight from (.*) to (.*) on the (.*) with a price of (\\d+) and a duration of (.*) provided by the company (.*)$")
    public void add_flight(String originCity, String destinationCity, String date, int price,
                           String duration, String company) {
        flights.put(new JSONObject().put("origin_city", originCity)
                                    .put("destination_city", destinationCity)
                                    .put("date", date)
                                    .put("price", price)
                                    .put("duration", duration)
                                    .put("company", company));
    }

    @Given("^a hotel stay in (.*) at the hotel (.*) from (.*) to (.*) for the price of (\\d+)$")
    public void add_hotel_stay(String city, String hotel, String dateStart, String dateEnd, int price) {
        hotelStays.put(new JSONObject().put("city", city)
                .put("hotel", hotel)
                .put("date_start", dateStart)
                .put("date_end", dateEnd)
                .put("price", price));
    }

    @Given("^a car rental in (.*) at the shop (.*) for a duration of (.*) and a price of (\\d+)$")
    public void add_car_rental(String city, String shop, String duration, int price) {
        carRentals.put(new JSONObject().put("city", city)
                .put("shop", shop)
                .put("duration", duration)
                .put("price", price));
    }

    @Given("^with (.*) set to (.*)$")
    public void add_request_attribute(String key, String value) { request.put(key.trim(),value);  }

    @Given("^a POI identified as (.*)$")
    public void perso_of_interest_ssn(String ssn) { this.ssn = ssn; }

    @Given("^the (.*) safe word$")
    public void setting_safe_word(String word) { this.safeWord = word; }

    @When("^the (.*) message is sent$")
    public void call_registry(String message) {
        JSONObject msg = new JSONObject();
        switch(message) {
            case "SUBMIT":
                if (flights.length() > 0)
                    request.put("flights", flights);

                if (carRentals.length() > 0)
                    request.put("car_rentals", carRentals);

                if (hotelStays.length() > 0)
                    request.put("hotel_stays", hotelStays);

                msg.put("event", message).put("request", request); break;
            case "VALIDATE":
                msg.put("event", message).put("id", ssn); break;
            case "REFUSE":
                msg.put("event", message).put("ssn", ssn); break;
            case "SUMMARY":
                msg.put("event", message).put("filter", filter); break;
            case "LIST":
                msg.put("event", message); break;
            case "PURGE":
                msg.put("event", message); break;
            default:
                throw new RuntimeException("Unknown message");
        }
        answer = call(msg);
        assertNotNull(answer);
    }

    @Then("^the submitted request id is (\\d+)$")
    public void the_request_id_is(int id) { assertEquals(id, answer.getInt("id")); }

    @Then("^the request is submitted$")
    public void the_request_is_submitted() {
        assertEquals("done",answer.getString("submit"));
    }


//    @Then("^the (.*) is equals to (.*)$")
//    public void check_request_content(String key, String value) {
//        Object data = answer.getJSONObject("citizen").get(key.trim());
//        if(data.getClass().equals(Integer.class)) {
//            assertEquals(Integer.parseInt(value.trim()), data);
//        } else {
//            assertEquals(value.trim(), data);
//        }
//    }

    @Then("^the (.*) of the (pending|approved) request (.*) is equals to (.*)$")
    public void check_list_request_attributes(String key, String status, String id, String value) {
        JSONObject req = answer.getJSONObject(status).getJSONObject(id);
        assertEquals(value, req.getString(key));
    }

    @Then("^the (pending|approved) request (.*) has (\\d+) (flights|hotel_stays|car_rentals)$")
    public void check_number_of_flights_car_hotel(String status, String id, int num, String type) {
        JSONObject req = answer.getJSONObject(status).getJSONObject(id);
        assertEquals(num, req.getJSONArray(type).length());
    }

    @Then("^there (?:are|is) (\\d+) pending request(?:s)? and (\\d+) approved request(?:s)? in the registry$")
    public void how_many_requests_in_the_registry(int pending, int approved) {
        JSONObject res = call(new JSONObject().put("event", "LIST"));
        assertEquals(pending,res.getJSONObject("pending").length());
        assertEquals(approved,res.getJSONObject("approved").length());
    }

}