Feature: Travel

  Background:
    Given an empty travel registry deployed on localhost:9060
      And a request for John added to the registry
      And a request for Jane added to the registry

  Scenario: Registering a travel request
    Given a travel request
      And with first_name set to Jean
      And with last_name set to Berger
      And a flight from Nice to Paris on the 12/02/2018 with a price of 100 and a duration of 1h20 provided by the company AirFrance
      And a car rental in Paris at the shop Mercedes for a duration of 5j and a price of 150
      And a car rental in Nice at the shop Garages et fils for a duration of 2j and a price of 50
      And a hotel stay in Nice at the hotel Le Pontif from 06/02/2018 to 07/02/2018 for the price of 40
      And a hotel stay in Nice at the hotel Le Pontif from 09/02/2018 to 11/02/2018 for the price of 90
      And a hotel stay in Paris at the hotel NomDhotel from 12/02/2018 to 17/02/2018 for the price of 150
    When the SUBMIT message is sent
    Then the request is submitted
      And the submitted request id is 3
      And there are 3 pending requests and 0 approved requests in the registry
    When the LIST message is sent
    Then the first_name of the pending request 3 is equals to Jean
      And the last_name of the pending request 3 is equals to Berger
      And the pending request 3 has 1 flights
      And the pending request 3 has 2 car_rentals
      And the pending request 3 has 3 hotel_stays