package fr.unice.polytech.esb.flows;

import fr.unice.polytech.esb.flows.utils.Endpoints;
import org.apache.camel.builder.RouteBuilder;

public class FindCheapestHotelTest extends ActiveMQTest {

    @Override public String isMockEndpointsAndSkip() { return Endpoints.HOTEL1_ENDPOINT; }

    @Override protected RouteBuilder createRouteBuilder() throws Exception { return new FindCheapestHotel(); }

//    @Test
    public void testFindCheapestHotel() throws Exception {

        // Asserting endpoints existence


        assertNotNull(context.hasEndpoint(Endpoints.GET_CHEAPEST_HOTEL));
        assertNotNull(context.hasEndpoint(Endpoints.HOTEL1_ENDPOINT));

        // Configuring expectations on the mocked endpoint
        assertNotNull(Endpoints.HOTEL1_ENDPOINT);

        String mock = "mock://"+Endpoints.HOTEL1_ENDPOINT;
        assertNotNull(context.hasEndpoint(mock));
        getMockEndpoint(mock).expectedMessageCount(1);
        getMockEndpoint(mock).expectedHeaderReceived("Content-Type", "application/json");
        getMockEndpoint(mock).expectedHeaderReceived("Accept", "application/json");
        getMockEndpoint(mock).expectedHeaderReceived("CamelHttpMethod", "GET");

        // Sending Johm for registration
        template.sendBody(Endpoints.GET_CHEAPEST_HOTEL,"Lyon");

        getMockEndpoint(mock).assertIsSatisfied();

        // As the assertions are now satisfied, one can access to the contents of the exchanges
        String request = getMockEndpoint(mock).getReceivedExchanges().get(0).getIn().getBody(String.class);
        System.out.println(request);
        //JSONAssert.assertEquals("", request, false);
    }

}
