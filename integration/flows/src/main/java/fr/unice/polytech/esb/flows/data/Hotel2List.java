package fr.unice.polytech.esb.flows.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Hotel2List implements Serializable {

    @JsonProperty("hotels") private List<Hotel2> hotels;

    public List<Hotel2> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel2> hotels) {
        this.hotels = hotels;
    }
}
