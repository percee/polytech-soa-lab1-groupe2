package fr.unice.polytech.esb.flows.utils;

public class AvionServiceHelper {

    public static String generateRequest(String src, String dest){
        return String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:avi=\"http://localhost/services/avion\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <avi:getFlights>\n" +
                "         <filtres>\n" +
                "            <src>%s</src>\n" +
                "            <dest>%s</dest>\n" +
                "         </filtres>\n" +
                "      </avi:getFlights>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>", src,dest);
    }

    public static String generateThirdPartyRequest(String src, String dest){
        return String.format("{\n" +
                "   \"event\":\"list\",\n" +
                "   \"departure\":\"%s\",\n" +
                "   \"destination\":\"%s\",\n" +
                "   \"departureTimeStamp\":710171030\n" +
                " }",src,dest);
    }

}
