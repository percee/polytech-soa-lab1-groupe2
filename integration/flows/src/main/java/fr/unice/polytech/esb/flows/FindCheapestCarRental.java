package fr.unice.polytech.esb.flows;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.unice.polytech.esb.flows.data.CarRental;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import static fr.unice.polytech.esb.flows.utils.Endpoints.*;

public class FindCheapestCarRental extends RouteBuilder {

    private static Logger logger = LoggerFactory.getLogger("Cheapest-Car-Log");


    @Override
    public void configure() throws Exception {
        restConfiguration().component("servlet");

        //http://localhost:8181/camel/rest/car/Paris
        rest("/car")
                .get("/{city}").to(GET_CHEAPEST_CAR_RENTAL);

        from(GET_CHEAPEST_CAR_RENTAL)
                .setHeader("CamelHttpPath",simple(""))
                .routeId("retrieve-cheapest-car-rental")
                .routeDescription("cc")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader("Content-Type", constant("application/json"))
                .setHeader("Accept", constant("application/json"))
                .multicast(mergeCars).parallelProcessing().executorService(Executors.newFixedThreadPool(1))
                .to(CHEAPEST_CAR1,CHEAPEST_CAR2)
                .end()
                .marshal().json(JsonLibrary.Jackson)
                .log("Cheapest car rental :")
                .log("${body}");

        from(CHEAPEST_CAR1)
                .routeId("cheapest-car1")
                .doTry()
                    .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                    .setHeader("Content-Type", constant("application/json"))
                    .setHeader("Accept", constant("application/json"))
                    .toD(CAR1_ENDPOINT + "/voiture/${header.city}?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .process(json2cars)
                    .process(cheapestCarFromList)
                .doCatch(UnknownHostException.class)
                    .log(LoggingLevel.ERROR,"In-house car rental not responding");

        from(CHEAPEST_CAR2)
                .routeId("cheapest-car2")
                .doTry()
                    .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                    .process(exchange -> exchange.getIn().setBody("{ \"city\" : \"${header.city}\" }"))
                    .toD(CAR2_ENDPOINT + "/car-service-document/car?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .process(json2cars2)
                    .process(cheapestCarFromList)
                .doCatch(UnknownHostException.class)
                    .log(LoggingLevel.ERROR,"Third-party car rental not responding");
    }

    private static Processor json2cars = (Exchange exchange) -> {
        Gson gson = new Gson();
        String s = exchange.getIn().getBody(String.class);
        Type collectionType = new TypeToken<ArrayList<CarRental>>(){}.getType();
        List<CarRental> cars = gson.fromJson(s, collectionType);
        exchange.getIn().setBody(cars);
    };

    private static Processor json2cars2 = (Exchange exchange) -> {
        Gson gson = new Gson();
        String s = exchange.getIn().getBody(String.class);
        logger.info("response from TP car: " + s);
        List<CarRental> outputCars = new ArrayList<>();
        // TODO : fill database & parse Json here
        exchange.getIn().setBody(outputCars);
    };

    private static Processor cheapestCarFromList = (Exchange exchange) -> {
        List<CarRental> cars = (List<CarRental>) exchange.getIn().getBody();
        double lowestPrice = Double.MAX_VALUE;
        CarRental cheapestCarRental = null;
        for (CarRental car: cars) {
            double price = car.getPrice();
            if(price < lowestPrice){
                lowestPrice = price;
                cheapestCarRental = car;
            }
        }
        exchange.getIn().setBody(cheapestCarRental);
    };

    private static AggregationStrategy mergeCars = (oldExchange, newExchange) -> {

        if (oldExchange == null) {
            return newExchange;
        }else{
            CarRental otherCar = newExchange.getIn().getBody(CarRental.class);
            CarRental car = oldExchange.getIn().getBody(CarRental.class);
            if (car.getPrice() < otherCar.getPrice()){
                oldExchange.getIn().setBody(car);
            }else{
                oldExchange.getIn().setBody(otherCar);
            }
        }
        return oldExchange;

    };
}
