package fr.unice.polytech.esb.flows.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
public class Flights implements Serializable {

    @JsonProperty("date") private Date date;
    @JsonProperty("duration") private int duration;
    @JsonProperty("price") private double price;
    @JsonProperty("company") private String company;
    @JsonProperty("src") private String src;
    @JsonProperty("dest") private String dest;

    @XmlElement
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    @XmlElement
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @XmlElement
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    @XmlElement
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    @XmlElement
    public String getSrc() {
        return src;
    }
    public void setSrc(String src) {
        this.src = src;
    }

    @XmlElement
    public String getDest() {
        return dest;
    }
    public void setDest(String dest) {
        this.dest = dest;
    }


    @Override
    public String toString() {
        return "Flights{" +
                "date=" + date +
                ", duration=" + duration +
                ", price=" + price +
                ", company='" + company + '\'' +
                ", src='" + src + '\'' +
                ", dest='" + dest + '\'' +
                '}';
    }
}
