package fr.unice.polytech.esb.flows.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Flights2 implements Serializable {
    @JsonProperty("departure")
    private Integer departure;
    @JsonProperty("numberOfFlights")
    private Integer numberOfFlights;
    @JsonProperty("ticketNo")
    private Integer ticketNo;
    @JsonProperty("arrival")
    private Integer arrival;
    @JsonProperty("seatClass")
    private String seatClass;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("_id")
    private Integer _id;
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("duration")
    private Integer duration;

    public Integer getDeparture() {
        return departure;
    }

    public void setDeparture(Integer departure) {
        this.departure = departure;
    }

    public Integer getNumberOfFlights() {
        return numberOfFlights;
    }

    public void setNumberOfFlights(Integer numberOfFlights) {
        this.numberOfFlights = numberOfFlights;
    }

    public Integer getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(Integer ticketNo) {
        this.ticketNo = ticketNo;
    }

    public Integer getArrival() {
        return arrival;
    }

    public void setArrival(Integer arrival) {
        this.arrival = arrival;
    }

    public String getSeatClass() {
        return seatClass;
    }

    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flights2 flights2 = (Flights2) o;

        if (departure != null ? !departure.equals(flights2.departure) : flights2.departure != null) return false;
        if (numberOfFlights != null ? !numberOfFlights.equals(flights2.numberOfFlights) : flights2.numberOfFlights != null)
            return false;
        if (ticketNo != null ? !ticketNo.equals(flights2.ticketNo) : flights2.ticketNo != null) return false;
        if (arrival != null ? !arrival.equals(flights2.arrival) : flights2.arrival != null) return false;
        if (seatClass != null ? !seatClass.equals(flights2.seatClass) : flights2.seatClass != null) return false;
        if (price != null ? !price.equals(flights2.price) : flights2.price != null) return false;
        if (_id != null ? !_id.equals(flights2._id) : flights2._id != null) return false;
        if (from != null ? !from.equals(flights2.from) : flights2.from != null) return false;
        if (to != null ? !to.equals(flights2.to) : flights2.to != null) return false;
        return duration != null ? duration.equals(flights2.duration) : flights2.duration == null;
    }

    @Override
    public int hashCode() {
        int result = departure != null ? departure.hashCode() : 0;
        result = 31 * result + (numberOfFlights != null ? numberOfFlights.hashCode() : 0);
        result = 31 * result + (ticketNo != null ? ticketNo.hashCode() : 0);
        result = 31 * result + (arrival != null ? arrival.hashCode() : 0);
        result = 31 * result + (seatClass != null ? seatClass.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (_id != null ? _id.hashCode() : 0);
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Flights2{" +
                "departure=" + departure +
                ", numberOfFlights=" + numberOfFlights +
                ", ticketNo=" + ticketNo +
                ", arrival=" + arrival +
                ", seatClass='" + seatClass + '\'' +
                ", price=" + price +
                ", _id=" + _id +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", duration=" + duration +
                '}';
    }
}

