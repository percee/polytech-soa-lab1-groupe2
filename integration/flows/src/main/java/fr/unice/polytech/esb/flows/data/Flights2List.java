package fr.unice.polytech.esb.flows.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Flights2List implements Serializable {

    @JsonProperty("flights") private List<Flights2> flights;

    public List<Flights2> getFlights() {
        return flights;
    }

    public void setFlights(List<Flights2> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        return "Flights2List{" +
                "flights=" + flights +
                '}';
    }
}
