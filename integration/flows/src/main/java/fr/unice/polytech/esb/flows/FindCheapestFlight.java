package fr.unice.polytech.esb.flows;

import com.google.gson.Gson;
import fr.unice.polytech.esb.flows.data.Flights;
import fr.unice.polytech.esb.flows.data.Flights2;
import fr.unice.polytech.esb.flows.data.Flights2List;
import fr.unice.polytech.esb.flows.utils.AvionServiceHelper;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeTimedOutException;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import static fr.unice.polytech.esb.flows.utils.Endpoints.*;

public class FindCheapestFlight extends RouteBuilder {

    private static Logger logger = LoggerFactory.getLogger("Cheapest-Flight-Log");

    //JaxbDataFormat jaxb = new JaxbDataFormat();

    @Override
    public void configure() throws Exception {
        //jaxb.setContextPath("fr.unice.polytech.esb.flows.data");

        restConfiguration().component("servlet");

        //http://localhost:8181/camel/rest/flight/London/Paris
        rest("/flight")
                .get("/{src}/{dest}").to(GET_CHEAPEST_FLIGHT);

        from(GET_CHEAPEST_FLIGHT)
                .setHeader("CamelHttpPath",simple(""))
                .routeId("retrieve-cheapest-flight")
                .routeDescription("cc")
                .multicast(mergeFlights).parallelProcessing().executorService(Executors.newFixedThreadPool(1))
                    .to(CHEAPEST_FLIGHT1,CHEAPEST_FLIGHT2)
                .end()
                .marshal().json(JsonLibrary.Jackson)
                .log("Cheapest Flight:")
                .log("${body}");

        from(CHEAPEST_FLIGHT1)
                .routeId("cheapest-flight1")
                .doTry()
                    .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                    .setHeader("Content-Type", constant("application/xml"))
                    .setHeader("Accept", constant("application/xml"))
                    .process((Exchange exchange) -> {
                        String src = exchange.getIn().getHeader("src",String.class);
                        String dest = exchange.getIn().getHeader("dest",String.class);
                        String soap = AvionServiceHelper.generateRequest(src,dest);
                        exchange.getIn().setBody(soap);
                    })
                    .inOut(FLIGHT1_ENDPOINT+"?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .process(soap2flights)
                    .process(cheapestFlight)
                .doCatch(UnknownHostException.class)
                    .setBody(simple(""))
                    .log(LoggingLevel.ERROR,"In-house flights not responding")
                .doCatch(ExchangeTimedOutException.class)
                    .setBody(simple(""))
                    .log(LoggingLevel.ERROR,"In-house flights timed out");

        from(CHEAPEST_FLIGHT2)
                .routeId("cheapest-flight2")
                .doTry()
                    .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                    .setHeader("Content-Type", constant("application/json"))
                    .setHeader("Accept", constant("application/json"))
                    .process((Exchange exchange) -> {
                        String src = exchange.getIn().getHeader("src",String.class);
                        String dest = exchange.getIn().getHeader("dest",String.class);
                        String json = AvionServiceHelper.generateThirdPartyRequest(src,dest);
                        exchange.getIn().setBody(json);
                    })
                    .inOut(FLIGHT2_ENDPOINT+"?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .process(json2flights)
                    .process(cheapestFlight)
                .doCatch(UnknownHostException.class)
                    .setBody(simple(""))
                    .log(LoggingLevel.ERROR,"Third-party flights not responding")
                .doCatch(ExchangeTimedOutException.class)
                    .setBody(simple(""))
                    .log(LoggingLevel.ERROR,"Third-party flights timed out");
    }

    private static Processor soap2flights = (Exchange exchange) -> {
        String s = exchange.getIn().getBody(String.class);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(s));
        Document doc = dBuilder.parse(is);
        NodeList nList = doc.getElementsByTagName("flights");
        List<Flights> flights = new ArrayList<>();
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                JAXBContext jaxbContext = JAXBContext.newInstance(Flights.class);
                Unmarshaller unmarshaler = jaxbContext.createUnmarshaller();
                Flights flight = (Flights) unmarshaler.unmarshal(nNode);
                logger.info("s: "+flight.getCompany());
                flights.add(flight);
            }
        }
        exchange.getIn().setBody(flights);
    };

    private static Processor json2flights = (Exchange exchange) -> {
        Gson gson = new Gson();
        String s = exchange.getIn().getBody(String.class);
        logger.info("2: " + s);
        Flights2List flights = gson.fromJson(s, Flights2List.class);
        List<Flights2> flight2s = flights.getFlights();
        List<Flights> outputFlights = new ArrayList<>();
        for (Flights2 flight: flight2s) {
            logger.info(flight.toString());
            Flights h = new Flights();
            h.setDate(new Date(flight.getDeparture()));
            h.setDuration(flight.getDuration());
            h.setPrice(flight.getPrice());
            h.setCompany("Unknown");
            h.setSrc(flight.getFrom());
            h.setDest(flight.getTo());
            outputFlights.add(h);
        }
        exchange.getIn().setBody(outputFlights);
    };

    private static Processor cheapestFlight = (Exchange exchange) -> {
        List<Flights> flights = (List<Flights>) exchange.getIn().getBody();
        Flights cheapestFlight = null;
        Double cheapestPrice = Double.MAX_VALUE;
        for (Flights flight : flights) {
            if(flight.getPrice() < cheapestPrice){
                cheapestFlight = flight;
                cheapestPrice = flight.getPrice();
            }
        }
        exchange.getIn().setBody(cheapestFlight);
    };

    private static AggregationStrategy mergeFlights = (oldExchange, newExchange) -> {

        if (oldExchange == null) {
            logger.info("p");
            Flights otherFlight = newExchange.getIn().getBody(Flights.class);
            logger.info(otherFlight.toString());
            return newExchange;
        }else{
            Flights otherFlight = newExchange.getIn().getBody(Flights.class);
            Flights flight = oldExchange.getIn().getBody(Flights.class);
            if (flight.getPrice() < otherFlight.getPrice()){
                oldExchange.getIn().setBody(flight);
            }else{
                oldExchange.getIn().setBody(otherFlight);
            }
        }
        return oldExchange;

    };

}