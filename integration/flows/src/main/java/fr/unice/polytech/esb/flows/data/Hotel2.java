package fr.unice.polytech.esb.flows.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Hotel2 implements Serializable {

    @JsonProperty("id") private Integer id;
    @JsonProperty("hotel_name") private String hotel_name;
    @JsonProperty("price_per_night") private Double price_per_night;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHotelName() {
        return hotel_name;
    }

    public void setHotelName(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public Double getPricePerNight() {
        return price_per_night;
    }

    public void setPricePerNight(Double price_per_night) {
        this.price_per_night = price_per_night;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hotel2 hotel = (Hotel2) o;

        if (id != null ? !id.equals(hotel.id) : hotel.id != null) return false;
        if (hotel_name != null ? !hotel_name.equals(hotel.hotel_name) : hotel.hotel_name != null) return false;
        return price_per_night != null ? price_per_night.equals(hotel.price_per_night) : hotel.price_per_night == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (hotel_name != null ? hotel_name.hashCode() : 0);
        result = 31 * result + (price_per_night != null ? price_per_night.hashCode() : 0);
        return result;
    }
}
