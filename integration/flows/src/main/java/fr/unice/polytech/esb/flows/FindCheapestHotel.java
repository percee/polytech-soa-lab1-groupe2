package fr.unice.polytech.esb.flows;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.unice.polytech.esb.flows.data.Hotel;
import fr.unice.polytech.esb.flows.data.Hotel2;
import fr.unice.polytech.esb.flows.data.Hotel2List;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import static fr.unice.polytech.esb.flows.utils.Endpoints.*;

public class FindCheapestHotel extends RouteBuilder {

    private static Logger logger = LoggerFactory.getLogger("Cheapest-Hotel-Log");

    @Override
    public void configure() throws Exception {

        restConfiguration().component("servlet");

        //http://localhost:8181/camel/rest/hotel/Paris
        rest("/hotel")
                .get("/{city}").to(GET_CHEAPEST_HOTEL);

        from(GET_CHEAPEST_HOTEL)
                .setHeader("CamelHttpPath",simple(""))
                .log("Cheapest hotel route called!")
                //.log("${headers}")
                .routeId("retrieve-cheapest-hotel")
                .routeDescription("cc")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader("Content-Type", constant("application/json"))
                .setHeader("Accept", constant("application/json"))
                .multicast(mergeHotels).parallelProcessing().executorService(Executors.newFixedThreadPool(1))
                    .to(CHEAPEST_HOTEL1,CHEAPEST_HOTEL2)
                .end()
                .marshal().json(JsonLibrary.Jackson)
                .log("Cheapest hotel :")
                .log("${body}");

        from(CHEAPEST_HOTEL1)
                .routeId("cheapest-hotel1")
                .doTry()
                    .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                    .setHeader("Content-Type", constant("application/json"))
                    .setHeader("Accept", constant("application/json"))
                    .toD(HOTEL1_ENDPOINT + "/hotel/${header.city}?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .process(json2hotel)
                    .process(cheapestHotelFromList)
                .doCatch(UnknownHostException.class)
                    .log(LoggingLevel.ERROR,"In-house hotels not responding");

        from(CHEAPEST_HOTEL2)
                .routeId("cheapest-hotel2")
                .doTry()
                    .toD(HOTEL2_ENDPOINT + "/tta-car-and-hotel/hotels/city/${header.city}?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .process(json2hotel2)
                    .process(cheapestHotelFromList)
                .doCatch(UnknownHostException.class)
                    .log(LoggingLevel.ERROR,"Third-party hotels not responding");

    }

    private static Processor json2hotel = (Exchange exchange) -> {
        Gson gson = new Gson();
        String s = exchange.getIn().getBody(String.class);
        logger.info("1: " + s);
        Type collectionType = new TypeToken<ArrayList<Hotel>>(){}.getType();
        List<Hotel> hotels = gson.fromJson(s, collectionType);
        logger.info("1: " + String.valueOf(hotels.size()));
        exchange.getIn().setBody(hotels);
    };

    private static Processor json2hotel2 = (Exchange exchange) -> {
        Gson gson = new Gson();
        String s = exchange.getIn().getBody(String.class);
        logger.info("2: " + s);
        Hotel2List hotels = gson.fromJson(s, Hotel2List.class);
        List<Hotel2> hotel2s = hotels.getHotels();
        logger.info("2: " + String.valueOf(hotels.getHotels().size()));
        List<Hotel> outputHotels = new ArrayList<>();
        for (Hotel2 hotel: hotel2s) {
            Hotel h = new Hotel();
            h.setId(hotel.getId());
            h.setName(hotel.getHotelName());
            h.setPrice(hotel.getPricePerNight());
            outputHotels.add(h);
        }
        exchange.getIn().setBody(outputHotels);
    };


    private static Processor cheapestHotelFromList = (Exchange exchange) -> {
        List<Hotel> hotels = (List<Hotel>) exchange.getIn().getBody();
        double lowestPrice = Double.MAX_VALUE;
        Hotel cheapesHotel = null;
        for (Hotel hotel: hotels) {
            double price = hotel.getPrice();
            if(price < lowestPrice){
                lowestPrice = price;
                cheapesHotel = hotel;
            }
        }
        exchange.getIn().setBody(cheapesHotel);
    };

    private static AggregationStrategy mergeHotels = (oldExchange, newExchange) -> {

        if (oldExchange == null) {
            return newExchange;
        }else{
                Hotel otherHotel = newExchange.getIn().getBody(Hotel.class);
                Hotel hotel = oldExchange.getIn().getBody(Hotel.class);
                if (hotel.getPrice() < otherHotel.getPrice()){
                    oldExchange.getIn().setBody(hotel);
                }else{
                    oldExchange.getIn().setBody(otherHotel);
                }
        }
        return oldExchange;

    };
}
