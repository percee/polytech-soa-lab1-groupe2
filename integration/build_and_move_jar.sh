#!/bin/sh

cd "`dirname $0`"

mvn clean package

cp flows/target/flows-1.0.jar ../deployment/service_mix_deploy

docker build -t bus .
