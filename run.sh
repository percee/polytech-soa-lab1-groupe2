#!/bin/sh
while true
do
	echo ""
	echo "Requesting Hotels on Nice"
	curl -G localhost:8181/camel/rest/hotel/Nice
	sleep 5s

	echo ""
	echo "Requesting Cars on Nice"
	curl -G localhost:8181/camel/rest/car/Nice
	sleep 5s

	echo ""
	echo "Requesting Hotels on NY"
	curl -G localhost:8181/camel/rest/hotel/New-York
	sleep 5s

	echo ""
	echo "Requesting Flights from NY to Paris"
	curl -G localhost:8181/camel/rest/flight/New-York/Paris
	sleep 5s

	echo ""
	echo "Requesting Flights from London to Paris"
	curl -G localhost:8181/camel/rest/flight/London/Paris
done