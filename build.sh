#!/usr/bin/env bash

cd services
echo "Building services"
./build.sh

cd ..

cd third-parties
echo "Building third-parties"
./build.sh

cd ..

cd integration
echo "Building integration"
./build.sh

echo "Done"
